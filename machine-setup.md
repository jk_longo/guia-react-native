Setup da sua máquina
====================

**Este tutorial foi escrito em fevereiro de 2019. Se algum erro ocorrer, considere verificar se este arquivo ficou desatualizado.**

Se você acreditar que sua máquina aguenta o Android Studio e aguenta emular um smartphone, siga [o tutorial oficial](https://facebook.github.io/react-native/docs/getting-started).

Instalando
----------

Antes de tudo, vamos instalar tudo que precisamos.

Como o React Native é uma biblioteca criada para permitir usar javascript no desenvolvimento de aplicativos mobile, nada mais justo que instalar as bibliotecas necessárias antes.

O que vamos precisar é do Java Development Kit 8 (JDK 8), do interpretador node e de seu gerenciador de pacotes (node package manager - npm) para então instalar as dependências do React Native. Os comandos de instalação estão abaixo:

```bash
# Instala o CURL se você não tiver
sudo apt install -y curl

# Baixa e instala o NVM
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash

# Reinicia as configurações do seu terminal para ele encontrar o nvm
source ~/.bashrc

# Instala a versão mais recente do node + npm
nvm install node

# Instala os comandos de terminal do react native
npm install -g react-native-cli

# Instala o JDK
sudo apt install -y openjdk-8-jdk openjdk-8-jre

# Pacote que sabe conversar com seu smartphone
sudo apt install -y adb
```

Agora, precisamos de uma ferramenta que nos permita emular um smartphone, ou que nos permita usar nosso próprio para testes.

Para emular um smartphone, é preciso utilizar o Android Studio. Você pode procurar um tutorial de como configurá-lo para utilizar com o React Native. Entretanto, certifique-se de que você tem RAM suficiente (pelos menos 8GB) e um processador decente para emular.

Como os computadores da Ecomp não tem nenhum dos dois requisitos, vamos precisar utilizar nosso próprio smartphone para desenvolver. Para isso, só precisamos instalar o SDK do Android Studio, e não o programa inteiro.

Para baixar a versão mais recente, entre no [site do Android Studio](https://developer.android.com/studio/?gclid=Cj0KCQiAnNXiBRCoARIsAJe_1cqIUTDm9XokrUshRidDXC-6y9hWoZQSDioZZcur6a4JglwEsx8LCAYaAuPfEALw_wcB) e baixe a versão Linux do SDK Tools:

![Download do SDK Tools do Android Studio](img/android-studio-sdk.png "Snapshot de exemplo")

Após baixar o SDK Tools, descompacte-o em uma pasta em sua home e prepare as variáveis de ambiente necessárias:

```bash
# Cria a pasta "android" em sua home se não existir
mkdir ~/android

# Descompacta o sdk-tools em ~/android/sdk (troque "meu-sdk-tools.zip" pelo nome do seu sdk tools!)
unzip -d ~/android/sdk meu-sdk-tools.zip

# Prepara o .bashrc para configurar as variáveis de ambiente
echo 'export ANDROID_HOME=$HOME/android/sdk' >> ~/.bashrc
echo 'export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin' >> ~/.bashrc
source ~/.bashrc
```

Depois de configurá-lo, só falta instalar os SDKs necessários para desenvolver suas aplicações.

Em fevereiro de 2019, a versão mais recente do React Native requer o SDK do android 28 (Pie), além de algumas outras dependências android. Este é o comando de instalação (você deverá aceitar os termos manualmente):

```bash
sdkmanager "add-ons;addon-google_apis-google-24" "build-tools;28.0.3" "platforms;android-28"
```

Depois de tudo instalado, você pode passar para o tutorial de desenvolvimento.

Chamadas de API
===============

Um elemento essencial de qualquer aplicação *web* ou *mobile* é um servidor que esteja executando a API da aplicação. Em aplicações *web*, as chamadas feitas à API são sob o mesmo domínio, ou seja, basta colocar qual é a rota (URL) da requisição e tudo vai funcionar beleza.

No caso de um aplicativo *mobile*, as coisas funcionam da mesma forma: basta colocar a rota da requisição e tudo vai funcionar beleza também.

Mas existem alguns problemas que sempre acabam confundindo os desenvolvedores. E são essas nuances que vamos cobrir aqui, junto com algumas dicas.

Como realizar as requisições
----------------------------

A primeira coisa é evitar utilizar as requisições `XMLHttpRequest` do *vanilla js* (*javascript* puro) e adotar uma biblioteca que simplifique o uso dessa interface.

A biblioteca mais utilizada recentemente é a [`axios`](https://github.com/axios/axios), que traz uma interface fácil de entender:

```js
import axios from "axios";

const url = "url.do.servidor.com.br/nome-da-rota";
const dados = { id: 1 };
const headers: { Authentication: "Bearer abcdefghijklmnopqrstuvwxyz" };
const configuracao = { data: dados, headers };

/* métodos disponíveis */
axios.get(url, configuracao).then(resposta => console.tron.log(resposta)).catch(erro => console.tron.log(erro));
axios.delete(url, configuracao).then(resposta => console.tron.log(resposta)).catch(erro => console.tron.log(erro));

axios.post(url, dados, { headers }).then(resposta => console.tron.log(resposta)).catch(erro => console.tron.log(erro));
axios.put(url, dados, { headers }).then(resposta => console.tron.log(resposta)).catch(erro => console.tron.log(erro));
axios.patch(url, dados, { headers }).then(resposta => console.tron.log(resposta)).catch(erro => console.tron.log(erro));

/* também é possível explicitar o método */
axios({ method: "POST", url: url, ...configuracao })
    .then(resposta => console.tron.log(resposta))
    .catch(erro => console.tron.log(erro));
```

A biblioteca já é bem grande, então se quiser mais detalhes [consulte a documentação oficial](https://github.com/axios/axios). Como de praxe, meu objetivo será lhe dar uma base para desenvolver, e nada mais.

Quando estamos lidando com APIs, o mais comum é que todas as rotas sejam do tipo `POST`.

Portanto, os métodos que são utilizados acabam sendo o `axios()` (genérico) e o `axios.post()` (para requisições POST, especificamente). O que vou usar é o método `axios()`.

Se você reparar nos exemplos acima, todas as chamadas contém duas chamadas "extras" depois de realizar as requisições, que são os métodos `.then()` e o `.catch()`.

Esses métodos são oriundos de [Promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) (promessas), que é uma interface criada para realizar **trabalhos assíncronos** (*i.e.* executar **funções assíncronas**).

Uma função assíncrona funciona como qualquer outra função que você aprendeu, mas com uma única diferença: ao chamá-la, a execução do programa principal **não para**. Para entender melhor, considere este exemplo:

```js
async function avisaComDelay() {
    /* retorna uma promessa que termina depois de dois segundos */
    return new Promise(resolve => 
        setTimeout(() => {
            console.log("Se passaram dois segundos!");
            resolve();
        }, 2000)
    );
}

function main() {
    console.log("Iniciando programa");
    console.log("Chamando avisaComDelay()");
    avisaComDelay();
    console.log("Continuando programa");
    /* restante */
}

main();
```

O que esse programa vai imprimir é:

```none
Iniciando programa
Chamando avisaComDelay()
Continuando programa
Se passaram dois segundos!
```

Ou seja, quando a função é chamada, o programa continua executando **sem saber quando ela vai terminar**.

Esse tipo de comportamento é extremamente útil para realizar requisições, porque não sabemos quanto tempo o servidor vai demorar para responder.

E se não sabemos, o melhor é utilizar o tempo de espera para manter o aplicativo responsivo ao usuário, certo? :)

É para isso que servem os métodos `.then()` e `.catch()`. O `.then()` é executado assim que a requisição termina **com sucesso**, e o `.catch()` é executado **caso algum erro seja retornado pelo servidor**, ao invés da resposta esperada.

Como definir a rota da requisição
---------------------------------

Se você estiver programando sua API com Laravel, as rotas são as mesmas. Ou seja, basta rodar `php artisan route:list` e checar que rotas existem, e quais são os métodos para acessá-las.

A única diferença aqui é que você precisa saber qual é o **host**, ou seja, qual é o **IP** ou **nome DNS** (ou **domínio**) do servidor. Quando estamos desenvolvendo localmente, nosso servidor é o computador, que não tem um nome DNS, e sim um IP.

E quando passamos para "produção" (quando a API vai pro ar de verdade, para uso comercial), com muita certeza seu servidor terá um **nome DNS**.

O que quero dizer com essa história? Aqui está o problema: Quando você está desenvolvendo, suas requisições são da forma `<ip-do-computador>/<rota-da-api>`. Entretanto, em produção as requisições deverão ser `<dominio>/<rota-da-api>`.

Como você faz sua aplicação lidar com isso programaticamente?

Existem várias formas. No nosso caso, se você leu o [Guia de Reactotron](./reactotron.md) antes deste, as coisas ficam bem mais simples: Basta utilizarmos nossos arquivos `env.js` e `env.example.js`:

```js
/* env.example.js */
let env;

if (__DEV__) {
    env = {
        ip: "ip-da-sua-maquina"
    };
} else {
    env = {
        ip: "dominio-do-servidor"
    };
}

export default env;
```

Esse esquema é super simples: Se seu aplicativo estiver em modo de desenvolvimento, o env utilizará o IP da sua máquina, e caso contrário, utilizará o domínio do servidor.

Com isso, quando você executar o comando de `build` (`npm run build`), sua aplicação já vai compilar com os dados corretos, sem precisar de ajustes manuais.

E como utilizar essa variável `env` nas requisições? Em resumo, como você precisa dar o caminho completo da requisição, suas chamadas ao `axios` ficarão assim:

```js
import env from "./caminho/para/env";

const rota = "rota-da-api";
const dados = {};

axios.post(env.ip + "/" + rota, dados);
```

Mas claro que ficar concatenando o IP em todo lugar do código é ruim. Por isso, recomendo que você faça um *wrapper* em cima disso:

```js
/* arquivo com as definições dos wrappers */
import env from "./caminho/para/env";

export function apiPost({ rota, dados, config, sucesso, falha }) {
    axios.post(`${env.ip}/${rota}`, dados, config).then(sucesso).catch(falha);
}

export function apiGet({ rota, config, sucesso, falha }) {
    axios.get(`${env.ip}/${rota}`, config).then(sucesso).catch(falha);
}
```

```js
/* no seu código */
import { apiPost } from "./caminho/para/wrappers";

/* ... */
apiPost({
    rota: "rota",
    dados: {},
    config: {},
    sucesso: function (resposta) {
        /* faz algo com resposta valida */
    },
    falha: function (erro) {
        /* faz algo com erro */
    },
});
```

Esquema de requisições com *Redux*
----------------------------------

Um **erro grave** extremamente comum com quem faz aplicações é **não impedir o usuário de fazer cagada**. Um problema recorrente é quando você faz uma tela que tem um botão que, ao ser clicado, manda uma requisição ao servidor.

Em casos em que essa requisição exige muito processamento do servidor, ele pode demorar um pouco pra responder. Mas o usuário **com certeza** não vai entender e vai simplesmente apertar o botão de novo, **repetindo a requisição e dobrando a demanda**.

Com *React* é bem fácil de resolver isso, e com *Redux* fica mais fácil ainda. Basta criar componentes que sabem verificar se o servidor já está trabalhando numa requisição ou não.

No guia sobre *Redux*, você aprendeu a criar ações, *reducers* e como conectá-los com seus componentes. O que vamos fazer é simples: Vamos criar um *reducer* **que gerencia nossa rede**, que será utilizado por componentes especiais.

Vou chamar esse *reducer* de `networkReducer()`, e vou criar um arquivo para ele junto com a *store*, ou seja, na pasta `ui/store/`:

```js
/* ui/store/network.js */
/* Coloquei as ações e o reducer juntos por preguiça :) */
import axios from "axios";
import { store } from "./store";
import env from "../../env";

/* actions */
const NetworkActionTypes = {
    REQUEST: "Network.REQUEST",
    REQUEST_SUCCESS: "Network.REQUEST_SUCCESS",
    REQUEST_FAILURE: "Network.REQUEST_FAILURE"
};

function request({ method, route, url, data, config, onSuccess, onError }) {
    onSuccess = onSuccess || function (response) { console.tron.log("SUCCESS", response); };
    onError = onError || function (error) { console.tron.log("ERROR", error); };
    data = data || {};
    config = config || {};


    store.dispatch({ type: NetworkActionTypes.REQUEST });
    axios({ method, url: url || `${env.ip}/${route}`, data, config })
    .then(response => {
        store.dispatch({ type: NetworkActionTypes.REQUEST_SUCCESS, payload: response });
        onSuccess(response);
    })
    .catch(error => {
        store.dispatch({ type: NetworkActionTypes.REQUEST_FAILURE, payload: error });
        onError(error);
    });
}

export function apiPost({ url, route, data, config, onSuccess, onError }) {
    request({
        method: "POST",
        url, route, data, config, onSuccess, onError
    });
}

export function apiGet({ url, route, config, onSuccess, onError }) {
    request({
        method: "GET",
        url, route, config, onSuccess, onError
    });
}

/* reducer */
const initialState = {
    onNetwork: false,
    payload: null,
};

export function reducer(state = initialState, action) {
    switch (action.type) {
        case NetworkActionTypes.REQUEST:
            return { ...state, onNetwork: true };
        case NetworkActionTypes.REQUEST_SUCCESS:
            return { ...state, onNetwork: false, payload: action.payload };
        case NetworkActionTypes.REQUEST_FAILURE:
            return { ...state, onNetwork: false, payload: action.payload };
        default:
            return state;
    }
}
```

```js
/* ui/store/reducers.js */
import { combineReducers } from "redux";

import { reducer as networkReducer } from "./network";
import homeReducer from "../screens/home/reducer";

export default combineReducers({
    network: networkReducer,
    home: homeReducer,
});
```

Pode parecer complicado, mas não é: Os método `apiPost()` e `apiGet()` fazem exatamente o que você acha: Eles fazem uma requisição POST e uma requisição GET, respectivamente.

A pegadinha fica nas três chamadas à `store.dispatch()`: Na primeira vez, o tipo de ação é "REQUEST", que significa que uma requisição foi enviada. Nesse ponto, o *reducer* atualiza seu estado dizendo que existe uma requisição pendente.

As duas chamadas `.then()` e `.catch()` executam duas instruções somente: Ambas despacham uma ação dizendo o resultado da requisição (se deu certo ou não, e qual o retorno), e em seguida executam o *callback* definido pelo desenvolvedor (se não forem passados, duas funções padrão são usadas).

Só com isso você tem controle das requisições e consegue fazer componentes que não deixam o usuário enviar muitas de uma vez. Um exemplo é este botão, que só deixa uma requisição passar por vez:

```js
/* ui/components/NetworkButton.js */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "react-native";

class NetworkButton extends Component {
    onPress = () => {
        if (this.props.onNetwork) {
            console.tron.log("ON NETWORK");
            return;
        }
        if (!this.props.onPress) return;

        this.props.onPress();
    }
    render() {
        return <Button {...this.props} onPress={this.onPress} />
    }
}

const mapStateToProps = (state) => ({
    onNetwork: state.network.onNetwork,
});

export default connect(mapStateToProps)(NetworkButton);
```

O método `onPress()` deve deixar bem claro: Se a aplicação já estiver esperando uma requisição, ele não executa a função `onPress()` repassada pelo usuário. Os nomes são iguais, mas são coisas diferentes!

Se você utilizar somente variações deste botão em sua aplicação toda, o usuário não vai conseguir nunca realizar mais que uma requisição apertando-o.

Se ele (ou você) tentar, é isso que será logado no Reactotron:

![Rede controlada com Redux](img/react-native-test-project-controlled-network.png "Rede controlada com Redux")

Lembrando que isso é só um esquema super básico, mas que já serve para aplicações de pequeno porte.

Se você não entendeu alguma parte, faça seus próprios testes. Mude as variáveis, funções e vá lendo as mensagens de erro. Entender como funciona a *framework* salva **muito** tempo. :)

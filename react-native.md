Guia de React Native
====================

**Nota pré-leitura:** Um projeto React / React Native escalável sempre tem:

1. Um gerenciador de estados global (Redux ou React Context);
2. Uma biblioteca de roteamento (React Router, React Native Router Flux);

Este guia não borda nenhum dos pontos acima. Mais informações sobre cada um estarão em guias separados, complementares à este.

O objetivo deste guia em específico é lhe dar uma noção do que é o React Native, e quais são os conceitos mais importantes (e como eles funcionam).

ReactJS
=======

Antes de falar sobre React Native, precisamos aprender um pouco sobre ReactJS.

Todos os conceitos que serão descritos aqui podem ser aplicados em React Native, salvo poucas restrições que serão abordadas [na seção sobre React Native](#react-native)

A explicação geral é simples: ReactJS é uma *framework* javascript cujo objetivo é facilitar como o DOM (elementos HTML) interage com o script. Por exemplo, falando de javascript "puro", considere o seguinte html:

```html
<input name="nome" type="text" placeholder="Nome" />
```

Se você quisesse saber o que o usuário digitou neste input, você precisaria de, no mínimo, dois passos:

1. Pesquisar este input específico e armazená-lo em uma variável X;
2. Pegar o valor do input X e (opcionalmente) salvar em Y.

O código resultante seria mais ou menos assim:

```jsx
var x = document.querySelector("input[name='nome']");
var y = x.value;
```

Simples o suficiente, certo?

Bom, com essa abordagem as coisas começam a ficar complicadas quando você tem que lidar com muitos e muitos elementos numa mesma página. O número de variáveis sobe e seu estado de confusão só aumenta.

Depois de um tempo, ninguém mais consegue dar manutenção ao código. O Midas é um ótimo exemplo (ele foi feito com JQuery).

Com ReactJS, **é possível** quebrar o problema em partes menores, facilitando a leitura do código. Mas antes de dar um exemplo, precisamos explicar alguns dos conceitos básicos.

Componentes
-----------

Esse é o conceito mais importante. Tudo em ReactJS funciona sobre "componentes".

Um componente é uma classe. E essa classe sempre tem um método chamado `render()`, cujo objetivo é retornar o HTML a ser renderizado. Renderizar significa escrever o HTML na página, mostrando-o ao usuário.

Um componente sempre herda a classe "Component" do React e tem o método `render()`. Aqui está um exemplo:

```jsx
// Digamos que o nome do arquivo seja "MeuComponente.js"
import React, { Component } from "react";

class MeuComponente extends Component {
    render() {
        return (
            <div>
                <p>Olá!</p>
            </div>
        );
    }
}

export default MeuComponente;
```

Nesse exemplo, temos um componente chamado "MeuComponente" que renderiza uma tag `<div>` com um paragrafo "Olá" dentro.

Com isso, podemos criar outro componente que usa nosso "MeuComponente":

```jsx
// OutroComponente.js
import React, { Component } from "react";
import MeuComponente from "./caminho/para/MeuComponente";

class OutroComponente extends Component {
    render() {
        return <MeuComponente />;
    }
}

export default OutroComponente;
```

Agora, temos um componente chamado "OutroComponente" que é mais inútil que o anterior: ele só renderiza o componente "MeuComponente", e mais nada.

Note que é necessário importar o componente "MeuComponente" **antes** de renderizá-lo, pois ele não está no mesmo arquivo que o "OutroComponente". Se estivessem no mesmo arquivo, o *import* não seria necessário. Ah, e o *import* só funciona porque fizemos o *export* na última linha do "MeuComponente".

Note também que **não é necessário** colocar a extensão do arquivo se ele for `.js`. Se for `.css` ou qualquer outra coisa, você **deve** especificar a extensão também.

Agora, antes de continuar, vamos falar de restrições do `render()`. A mais comum, que sempre gera problemas para novatos, é que esse método deve renderizar **um único elemento DOM**. Ou seja, um componente que tenta renderizar mais de um elemento não é permitido:

```jsx
// ComponenteErrado.js
import React, { Component } from "react";
import MeuComponente from "./caminho/para/MeuComponente";

class ComponenteErrado extends Component {
    render() {
        return (
            <MeuComponente />
            <MeuComponente />
        );
    }
}

export default ComponenteErrado;
```

Isso acontece para evitar que o React se perca na hora de comparar mudanças para atualizar as páginas. Para consertar, é extremamente simples: Basta colocar tudo dentro de um elemento neutro ("invisível"), como um `<div>`:

```jsx
// ComponenteCerto.js
import React, { Component } from "react";
import MeuComponente from "./caminho/para/MeuComponente";

class ComponenteCerto extends Component {
    render() {
        return (
            <div>
                <MeuComponente />
                <MeuComponente />
            </div>
        );
    }
}

export default ComponenteCerto;
```

Com isso, você estará renderizando um único elemento. :)

Props
-----

Esse é um conceito que dá sentido à existência de componentes. Como você deve ter visto, separar a aplicação em componentes que sempre renderizam a mesma coisa não ajuda tanto assim.

Por isso, é possível repassar informações de um componente pai para seus componentes filhos, através de *props*. Para entender melhor, considere o seguinte componente:

```jsx
import React, { Component } from "react";

class Mensagem extends Component {
    render() {
        return (
            <div>
                <h1>{ this.props.titulo || "Título padrão" }</h1>
                <p>{ this.props.mensagem || "Mensagem padrão" }</p>
            </div>
        );
    }
}

export default Mensagem;
```

O componente "Mensagem" aceita dois props, chamados "titulo" e "mensagem", que são opcionais. O operador `||` em javascript não é só um operador lógico: o código `this.props.titulo || "Título Padrão"` pode ser lido como "se o atributo titulo não estiver definido, use a string 'Título Padrão' no lugar".

Se o código fosse `<h1>{ this.props.titulo }</h1>`, o "titulo" se tornaria obrigatório, pois caso contrário, o React tentará renderizar o valor "undefined" (que é retornado quanto uma variável que não existe é utilizada), e isso causará um erro de renderização.

Note que para utilizar variáveis dentro do "HTML" (pesquise sobre JSX se estiver curioso), é preciso utilizar chaves `{}` para que o React saiba que ali dentro tem javascript para ser executado.

Agora, para utilizar esse componente, vamos criar um outro que renderiza mensagens:

```jsx
import React, { Component } from "react";
import Mensagem from "./caminho/para/Mensagem";

class Mensageiro extends Component {
    render() {
        return (
            <div>
                <Mensagem />
                <Mensagem titulo="Olá mundo" />
                <Mensagem mensagem="Estou testando esse componente" />
                <Mensagem titulo="Olá mundo" mensagem="Estou testando esse componente" />
            </div>
        );
    }
}

export default Mensageiro;
```

O componente "Mensageiro" resultará nesse HTML:

```html
<div>
    <div>
        <h1>Título padrão</h1>
        <p>Mensagem padrão</p>
    </div>
    <div>
        <h1>Olá mundo</h1>
        <p>Mensagem padrão</p>
    </div>
    <div>
        <h1>Título padrão</h1>
        <p>Estou testando esse componente</p>
    </div>
    <div>
        <h1>Olá mundo</h1>
        <p>Estou testando esse componente</p>
    </div>
</div>
```

Também é possível passar variáveis para componentes, através das *props*. Basta utilizar chaves `{}` na hora de passar o valor da *prop*:

```jsx
import React, { Component } from "react";
import Mensagem from "./caminho/para/Mensagem";

class Mensageiro extends Component {
    render() {
        let titulo = "Olá mundo";
        let minhaMensagem = "Estou testando esse componente";
        return (
            <div>
                <Mensagem />
                <Mensagem titulo={titulo} />
                <Mensagem mensagem={minhaMensagem} />
                <Mensagem titulo={titulo} mensagem={minhaMensagem} />
            </div>
        );
    }
}

export default Mensageiro;
```

A variação acima do componente "Mensageiro" resulta no mesmo HTML. Note que não tem problema o nome da variável ser igual ou não ao nome da *prop*.

As *props* podem ser objetos, vetores, funções e o que mais for possível passar através de uma variável. Basta você saber como utilizar.

### Adendo sobre chaves

Dentro das chaves `{}` deve-se colocar **um único *statement***. Esse *statement* pode ser uma chamada de função, uma expressão lógica, etc. E não, um laço "for" ou "while" não é um único *statement*.

Renderizando vetores (arrays)
-----------------------------

Essa deve ser a funcionalidade mais útil para componentes. Um caso de uso imediato é na renderização de uma tabela, que possui várias linhas não-estáticas (*i.e.* dinâmicas).

Mas como não é possível utilizar laços "for" e "while" dentro de chaves `{}`, como vamos renderizar um vetor? Simples: lembre que um Array em javascript é, na realidade, um objeto, e portanto, basta utilizarmos o método "map()" que vem com ele:

```jsx
import React, { Component } from "react";
import Mensagem from "./caminho/para/Mensagem";

class Mensageiro extends Component {
    render() {
        let mensagens = [
            { id: 1, titulo: "O", mensagem: "Sou a primeira mensagem!" },
            { id: 2, titulo: "Ol", mensagem: "Sou a segunda mensagem!" },
            { id: 3, titulo: "Olá", mensagem: "Sou a terceira mensagem!" },
            { id: 4, titulo: "Olá!", mensagem: "Sou a quarta mensagem!" },
        ];
        return (
            <div>
            {
                /* método correto 1 */
                mensagens.map(mensagem => <Mensagem key={mensagem.id} titulo={mensagem.titulo} mensagem={mensagem.mensagem} />)
            }
            {
                /* método correto 2 */
                mensagens.map(function (mensagem) {
                    return <Mensagem key={mensagem.id} titulo={mensagem.titulo} mensagem={mensagem.mensagem} />;
                })
            }
            {
                /* método errado 1 */
                mensagens.forEach(mensagem => <Mensagem key={mensagem.id} titulo={mensagem.titulo} mensagem={mensagem.mensagem} />)
            }
            {
                /* método errado 2 */
                mensagens.forEach(function (mensagem) {
                    return <Mensagem key={mensagem.id} titulo={mensagem.titulo} mensagem={mensagem.mensagem} />;
                })
            }
            </div>
        );
    }
}

export default Mensageiro;
```

Vamos analisar por partes:

* A variável `mensagens` é um array `[]` de objetos `{}`, onde cada objeto tem um ID, titulo e mensagem;

* Os métodos corretos 1 e 2 são exatamente idênticos, assim como os métodos errados 1 e 2. A diferença é que eu usei um jeito diferente de representar uma função nos "método correto 1" e "método errado 1" (leia [este artigo](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions));

* **Lembre para toda a vida**: para renderizar um array `[]`, é necessário sempre passar um *prop* extra chamado `key`, que **deve ser único para cada elemento renderizado**. No exemplo, eu dei IDs fictícios para cada elemento. Há pessoas que utilizam o índice do elemento no array `[]` como *key*, mas [isso pode gerar muita dor de cabeça (leia este artigo)](https://medium.com/@robinpokorny/index-as-a-key-is-an-anti-pattern-e0349aece318);


* O método "map()" do Array deve receber uma função como argumento, assim como o método "forEach()";

* O método "map()" retorna um "mapeamento" do array `[]` na forma de um novo array `[]`, ou seja, ele transforma cada elemento em outro de acordo com a função passada como argumento. No exemplo, o "método correto 1" e o "método correto 2" fazem cada objeto do array `[]` de mensagens virar um componente "Mensagem";

* O método "forEach()" não retorna nada, ele só itera pelos elementos do vetor e executa a função passada como argumento sobre cada um dos elementos. Ou seja, os *returns* que coloquei no exemplo são inúteis;

* Para renderizar um array `[]`, deve-se retornar um array `[]` de elementos para o React renderizar. Como o "forEach()" não retorna "nada" (funções que não retornam nada devolvem `undefined`, na verdade), o React não recebe nada para renderizar, resultando no mesmo erro que discutimos acima.

Resumindo, a variante do componente "Mensageiro" acima causará erro ao renderizar, por conta dos métodos errados 1 e 2. Se eles forem retirados, o resultado será dois "blocos" de quatro mensagens, sendo ambos os "blocos" idênticos.

Métodos
-------

Como um componente é uma classe, obviamente podemos criar métodos auxiliares para fazer as coisas funcionarem.

Por exemplo, esse é um componente que usa métodos auxiliares para renderizar:

```jsx
import React, { Component } from "react";

class Alerta extends Component {
    constructor(props) {
        super(props); /* essa linha é padrão e obrigatória, sempre coloque */

        /* aqui estamos forçando o elemento "this" sempre representar o nosso componente */
        this.renderizaTitulo = this.renderizaTitulo.bind(this);
        this.renderizaMensagem = this.renderizaMensagem.bind(this);
        this.renderizaAlerta = this.renderizaAlerta.bind(this);
    }

    renderizaTitulo(titulo) {
        return <h1>{titulo}</h1>;
    }

    renderizaMensagem(mensagem) {
        return <p>{mensagem}</p>;
    }
    
    renderizaAlerta(titulo = "Sem título", mensagem = "Sem mensagem") {
        return (
            <div>
            {this.renderizaTitulo(titulo)}
            {this.renderizaMensagem(mensagem)}
            </div>
        );
    }

    render() {
        return (
            <div>
            {this.renderizaAlerta(this.props.titulo, this.props.mensagem)}
            </div>
        );
    }
}

export default Alerta;
```

Como você pode ver, quando o método `render()` é executado, vários outros métodos são chamados para auxiliar na renderização. Isso é útil em casos onde você tem que fazer a mesma coisa várias vezes, como renderizar linhas em uma tabela.

A magia negra fica no `constructor()`. Sempre que você decidir incluir métodos auxiliares no seu componente, crie também o `constructor(props)`, e coloque o *statement* `super(props)` na primeira linha. O que isso faz é chamar o `constructor()` da classe "Component", da qual todos os nossos componentes herdam (lembra da parte `extends Component`?).

Assim que o construtor da classe "Component" termina, você fica livre para fazer o que quiser no construtor. Ele é chamado uma única vez, que é quando seu componente é renderizado na tela.

O primeiro passo recomendado é sempre "ligar fortemente" o seu componente aos seus métodos, para que a *keyword* `this` **sempre** referencie seu componente, e não algo diferente.

Se você não entendeu o último parágrafo, dê uma revisada nos conceitos de orientação à objetos (o que é o `this`, especificamente) e depois continue a leitura.

Um outro modo de ligar fortemente seus métodos ao seu componente é utilizando *arrow functions* para criá-los:

```jsx
import React, { Component } from "react";

class Alerta extends Component {
    renderizaTitulo = (titulo) => {
        return <h1>{titulo}</h1>;
    }

    renderizaMensagem = (mensagem) => {
        return <p>{mensagem}</p>;
    }
    
    renderizaAlerta = (titulo = "Sem título", mensagem = "Sem mensagem") => {
        return (
            <div>
            {this.renderizaTitulo(titulo)}
            {this.renderizaMensagem(mensagem)}
            </div>
        );
    }

    render() {
        return (
            <div>
            {this.renderizaAlerta(this.props.titulo, this.props.mensagem)}
            </div>
        );
    }
}

export default Alerta;
```

Repare na diferença sutil no modo de criar um método. Você decide qual das duas opções utilizar. Se quiser uma explicação mais detalhada, [leia este artigo](https://medium.freecodecamp.org/the-best-way-to-bind-event-handlers-in-react-282db2cf1530).

Eventos
-------

Assim como toda *framework*, o ReactJS também conta com uma série de utensílios para te ajudar a cuidar de eventos (cliques, *hovers*, valores digitados, ...).

Para lidar com eventos, basta programar um *EventHandler* (que é só uma função que recebe argumentos pré-definidos).

Tudo o que você precisa saber [está aqui](https://reactjs.org/docs/handling-events.html). O que vou passar é uma versão simplificada.

Os eventos mais comuns são o clique do mouse, ou o preenchimento de *inputs* (quando o usuário seleciona uma opção ou preenche um campo de um formulário). São esses que vamos aprender.

Um *EventHandler* para clique é o mais simples: é uma função que recebe um único argumento, que é um `SyntheticEvent`. Não vamos usá-lo, então nosso *EventHandler* não receberá nenhum argumento.

Como de praxe, eis um exemplo de um componente que escreve "Fui clicado!" no console quando o botão é clicado:

```jsx
import React, { Component } from "react";

class BotaoInutil extends Component {
    escreveNoConsole = (mensagem) => {
        console.log(mensagem);
    }

    aoClicar = () => {
        this.escreveNoConsole("Fui clicado!");
    }

    render() {
        return <button onClick={this.aoClicar}>Me clique</button>;
    }
}

export default BotaoInutil;
```

Como o método `aoClicar()` utiliza outro método dentro dele, é fortemente recomendado que você ligue-o fortemente ao seu componente, através de um `bind(this)` no construtorm, ou através de *arrow functions*. Se você não fizer isso, você sofrerá com um erro dizendo que não existe um método chamado "escreveNoConsole()" ao clicar no botão.

Tem pessoas que realizam a ligação direto no `render()`, assim:

```jsx
import React, { Component } from "react";

class BotaoInutil extends Component {
    escreveNoConsole(mensagem) {
        console.log(mensagem);
    }

    aoClicar() {
        this.escreveNoConsole("Fui clicado!");
    }

    render() {
        return <button onClick={this.aoClicar.bind(this)}>Me clique</button>;
    }
}

export default BotaoInutil;
```

Isso também funciona. **Mas nunca faça isso**. **Nunca**.

Por que? Isso [causa problemas de performance que podem se tornar muito graves](https://medium.freecodecamp.org/the-best-way-to-bind-event-handlers-in-react-282db2cf1530) (é o mesmo artigo que citei agora pouco), por conta de "rerenderização" de partes que não precisam ser atualizadas.

Outra prática muito comum é utilizar *arrow functions* como *EventHandler*, diretamente no `render()`:

```jsx
import React, { Component } from "react";

class BotaoInutil extends Component {
    escreveNoConsole(mensagem) {
        console.log(mensagem);
    }

    render() {
        return <button onClick={() => this.escreveNoConsole("Fui clicado!")}>Me clique</button>;
    }
}

export default BotaoInutil;
```

De novo, isso também funciona, mas pelo mesmo motivo do último exemplo, **evite fazer isso**. Prefira sempre uma das duas opções que passei.

Bom, depois de tanto sermão usando o *onClick* como exemplo, podemos passar para segundo evento mais comum, que é o de alterações em *inputs*.

Um *EventHandler* de um *input* recebe dois argumentos, nessa ordem: Um `SyntheticEvent`, geralmente nomeado de `event`, e o elemento que disparou o evento, geralmente nomeado de `data`.

A *prop* de um evento de *input* se chama *onChange*. Eis mais um exemplo:

```jsx
import React, { Component } from "react";

class CampoTextoInutil extends Component {
    aoAlterar = (evento, dado) => {
        let valorEscritoAtualmente = dado.value;
        let nomeDoInput = dado.name;
        console.log(`O input "${nomeDoInput}" tem o valor ${valorEscritoAtualmente}`);
        // Faz algo com isso
    }

    render() {
        return <input type="text" name="algumNome" onChange={this.aoAlterar} />
    }
}

export default CampoTextoInutil;
```

Geralmente não usamos o `SyntheticEvent` para nada, então ele só fica ali como primeiro argumento. Se você está pensando "e se a gente tirar ele como argumento e deixar só o dado?", então você está indo para uma fria: O primeiro argumento, **não importa qual nome você dê pra ele**, vai ser sempre o `SyntheticEvent`.

Se eu chamasse meu argumento "evento" de "batata", ele ainda representaria a mesma coisa. O nome de um argumento no seu método não altera o que é passado para ele.

Para cuidarmos do estado de um formulário (o que está escrito/selecionado nele), precisamos aprender *Redux*. O método mais simples é utilizando o estado interno de um componente, mas não é recomendado que sua aplicação tenha os dois presentes. Ou você usa estados internos, ou você usa o *Redux* para manter o estado global.

Lifecycle
---------

Um outro conceito muito utilizado é o ciclo de vida de um componente.

Ele funciona através de *callbacks* (métodos do componente que são chamados por fora quando um evento ocorre) com nomes pré-determinados.

Para dar um exemplo mais detalhado, precisamos entender o que é *Redux* antes, pois não queremos utilizar o estado interno de um componente. Então, só vou passar aqui a lista com explicações sobre os mais comuns ([a lista completa está aqui](https://reactjs.org/docs/react-component.html)):

```jsx
import React, { Component } from "react";

class ComponenteExemplo extends Component {
    /*
        evento mais simples de entender.
        o construtor é chamado quando o componente é criado.
     */
    constructor(props) {
        super(props);
        /* aqui, você inicializa atributos ou analisa as props passadas pela primeira vez.  */
    }

    /*
        chamado assim que o componente termina de ser
        renderizado na página.

        útil para iniciar timers e afins, como por exemplo,
        contar quanto tempo o usuário ficou na página, usando
        este método para marcar o horário inicial e o método
        "componentWillUnmount()" para marcar o horário final.
     */
    componentDidMount() {
        this.initialTime = new Date();
    }

    /* 
        chamado antes do componente ser retirado do DOM.
     */
    componenteWillUnmount() {
        this.endTime = new Date();
        let timeInPage = endTime - initialTime;
        this.saveThisTime(timeInPage);
    }

    /* 
        chamado antes do componente ser atualizado.
        
        ele permite que você analise informações antes do componente
        ser atualizado, como a posição do scroll ou algo relacionado
        ao componente.

        esse método deve retornar um valor null ou as informações que
        você quiser retornar.

        esse valor será enviado como o argumento snapshot do método
        "componentDidUpdate()".
     */
    getSnapshotBeforeUpdate(prevProps, prevState) {
        let { batata, banana } = prevProps;
        let snapshot = {
            batata,
            banana,
            temBatataEBanana: batata !== null && banana !== null
        };
        return snapshot;
    }

    /* 
        chamado assim que o componente termina de ter seu estado
        ou suas props atualizados.

        aqui, você sabe que o this.props ou o this.state foi modificado.
        você também tem acesso às props e estado anteriores.

        com Redux, você não utiliza o estado (apenas as props).
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot && snapshot.temBatataEBanana) {
            this.analyzeDifferencesInProps(prevProps, this.props);
            this.activateLasers();
        }
    }
}

export default ComponenteExemplo;
```

De novo, se quiser mais detalhes, [veja a lista oficial](https://reactjs.org/docs/react-component.html).

Conceitos importantes de React que não serão passados
-----------------------------------------------------

Motivo? Em React Native é diferente, ou você não deve usá-lo junto com *Redux*.

Eis a lista:

* [Component state](https://reactjs.org/docs/state-and-lifecycle.html) (não deve ser usado com *Redux*);
* [Style and Classes](https://reactjs.org/docs/faq-styling.html) (em React Native é diferente);
* [React Context](https://reactjs.org/docs/context.html) (não deve ser usado com *Redux*, ou deve ser integrado com *Redux*);

React Native
============

Repare na quantidade de conteúdo sobre ReactJS que você acabou de ler ou pular.

Salvo algumas diferenças, tudo isso funciona em React Native da mesma forma. Por isso, essa seção cobrirá basicamente quais são essas diferenças e que cuidados extras você deve tomar.

Renderizando componentes
------------------------

A primeira e maior diferença de ReactJS para React Native é que você **não usa tags HTML**. Ou seja, nada de `<div>`, `<a>`, `<p>`, `<h1>`, `<h2>`, `<h3>`, `<h4>`, `<h5>`, `<h6>`.

O que usamos no lugar de tags são componentes do React Native, que você importa do pacote "react-native":

```jsx
import React, { Component } from "react";
import { View, Text } from "react-native";

class ComponenteExemplo extends Component {
    render() {
        return (
            <View>
                <Text>Olá!</Text>
            </View>
        );
    }
}

export default ComponenteExemplo;
```

Veja [a documentação oficial](https://facebook.github.io/react-native/docs/components-and-apis) para mais informações sobre cada componente.

Aqui vai uma tabela que simplifica qual componente usar no lugar de uma tag (clique no componente para ir para a documentação oficial):

Tag HTML                                              | Componente React Native
------------------------------------------------------|------------------------------------------------------
`<div>, <main>, <section>, <footer>, <nav>, <header>` | [`<View>`](https://facebook.github.io/react-native/docs/view.html), [`<ScrollView>`](https://facebook.github.io/react-native/docs/scrollview.html), [`<KeyboardAvoidingView>`](https://facebook.github.io/react-native/docs/keyboardavoidingview.html)
`<p>, <h1>, <h2>, <h3>, <h4>, <h5>, <h6>`             | [`<Text>`](https://facebook.github.io/react-native/docs/text.html)
`<img>`                                               | [`<Image>`](https://facebook.github.io/react-native/docs/image.html)
`<input type="text">`                                 | [`<TextInput>`](https://facebook.github.io/react-native/docs/textinput.html)
`<button>, <a>`                                       | [`<Button>`](https://facebook.github.io/react-native/docs/button.html)
`<input type="radio">`                                | [`<Switch>`](https://facebook.github.io/react-native/docs/switch.html)
`<input type="range">`                                | [`<Slider>`](https://facebook.github.io/react-native/docs/slider.html)
`<select>, <option>` (opção única)                    | [`<Picker>, <Picker.Item>`](https://facebook.github.io/react-native/docs/picker.html)
`<ol>, <ul>`                                          | [`<FlatList>`](https://facebook.github.io/react-native/docs/flatlist.html), [`<SectionList>`](https://facebook.github.io/react-native/docs/sectionlist.html)
`<input type="date">`                                 | [`<DatePickerIOS>`](https://facebook.github.io/react-native/docs/datepickerios.html),  [`<DatePickerAndroid>`](https://facebook.github.io/react-native/docs/datepickerandroid.html) (esse não é um componente!)

Note que alguns componentes React Native só funcionam para uma plataforma (iOS ou Android). Portanto, se você quiser criar um APP que funcione em ambas as plataformas, você deve aprender a renderizar o componente correto dada a plataforma alvo (veja [esta seção para mais informações](#trabalhando-com-aplicativos-multi-plataforma)).

User Interface
--------------

Para estilização da UI, eu sugiro fortemente que você decida com sua equipe (se não estiver sozinho) sobre usar **uma única biblioteca de UI**. É muito melhor utilizar componentes padronizados prontos do que fazer tudo do zero.

**Sempre** prefira bibliotecas feitas sobre o padrão [**Material Design**](https://material.io/) da Google. Com isso, seus botões, ícones e toda a interface será extremamente confortável ao usuário, simplesmente por ser similar a qualquer outro aplicativo.

Com isso, minhas recomendações são [react-native-material-ui](https://github.com/xotahal/react-native-material-ui) ou o [NativeBase](https://nativebase.io/) ([github aqui](https://github.com/GeekyAnts/NativeBase)).

Mas com certeza você precisará realizar uma ou outra alteração na UI para adaptá-la ao gosto do seu cliente. Por isso, vamos aprender como funciona a estilização de componentes em React Native.

Infelizmente, atualmente não existe meios de utilizar CSS "puro" em React Native. Entretanto, para contornar isso, [uma interface que "simula" CSS foi criada](https://facebook.github.io/react-native/docs/stylesheet.html).

Nem toda regra CSS existe nessa interface. Além disso, cada componente React Native tem seu próprio conjunto de "*styles*" que podem ser aplicados (sempre procure a prop "style" na documentação do componente para ver quais podem ser utilizados).

<img src="img/react-native-component-props.png" width="1200" height="600">
<img src="img/react-native-component-style.png" width="1200" height="600">

Você pode definir estilos do seu componente de duas maneiras: incluindo-o diretamente na *prop* "*style*" (assim como incluimos CSS diretamente no atributo "style" de uma tag), ou através de uma `StyleSheet`.

Sempre evite colocar estilos diretamente na *prop*, pois isso dificulta a reutilização do estilo depois.

A forma mais simples de criar uma `StyleSheet` é colocando-a no mesmo arquivo do componente:

```jsx
import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

class ComponenteEstilizado extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.texto}>Estou estilizado!</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#000",
    },
    texto: {
        color: "#FFF",
    },
});

export default ComponenteEstilizado;
```

Note que existe uma pequena diferença na nomenclatura dos estilos: Ao invés de utilizar hífen `-`, você utiliza `camelCase`. No exemplo acima, o estilo `background-color` vira `backgroundColor`.

Lembre-se que aqui estamos falando de javascript. Então para repassar a cor `#000` você precisa utilizar uma string (ou seja, não esqueça das aspas!).

Boas práticas: a maioria dos projetos feitos em React Native separam a `StyleSheet` do componente em um arquivo separado, assim como fazemos com CSS, pois ela pode ficar bem grande. Para isso, basta separar a variável "styles" (não precisa ser esse nome obrigatoriamente) em um arquivo, exportá-la de lá e importá-la no seu componente.

Com isso, você pode criar, por exemplo, um arquivo de "styles" genéricos, para importá-los em vários componentes.

O padrão recomendado é o seguinte: Se o arquivo de estilos for específico do seu componente, nomeie-o com o mesmo nome do componente, com o sufixo "Styles":

```jsx
/* arquivo ComponenteEstilizadoStyles.js */
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#000",
    },
    texto: {
        color: "#FFF",
    },
});

export default styles;
```

```jsx
/* arquivo ComponenteEstilizado.js */
import React, { Component } from "react";
import { View, Text } from "react-native";

/* assumindo que o arquivo está na mesma pasta, no mesmo nível */
import styles from "./ComponenteEstilizadoStyles";

class ComponenteEstilizado extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.texto}>Estou estilizado!</Text>
            </View>
        );
    }
}

export default ComponenteEstilizado;
```

Trabalhando com aplicativos multi-plataforma
--------------------------------------------

Não é nada muito complicado. O React Native facilita muito as coisas.

Existe uma classe chamada `Platform` que lhe permite definir o que usar para cada plataforma:

```jsx
import React, { Component } from "react";
import { View, TouchableHighlight, Text, DatePickerIOS, DatePickerAndroid, Platform } from "react-native";

/*
    o DatePickerAndroid não é um componente React, mas o DatePickerIOS sim.
    o workaround é criar um componente React para o DatePickerAndroid.
 */
class DatePickerAndroidComponent extends Component {
    /*
        não devia usar state, mas nesse caso
        só estou usando para mostrar que data
        foi selecionada. :)
     */
    state = { dateSelected: new Date() };

    constructor(props) {
        super(props);

        this.date = props.date || new Date();
        this.onDateSelected = props.onDateSelected || function (date) {};
    }

    /*
        o DatePickerAndroid é assíncrono, então nosso método
        também deve ser.

        quando temos uma função "async", podemos utilizar dentro dela a
        keyword "await", que não deixa o restante do código ser executado
        enquanto a função chamada não retornar.

        entretanto, qualquer código FORA dessa função continuará executando
        normalmente.
     */
    getDateFromUser = async () => {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
                // Use `new Date()` for current date.
                // May 25 2020. Month 0 is January.
                date: new Date(2020, 4, 25)
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                let selection = new Date(year, month + 1, day);
                this.setState({ dateSelected: selection });
                this.onDateSelected(selection);
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    }

    render() {
        return (
            <TouchableHighlight style={{ borderWidth: 1, borderColor: "#000" }} onPress={this.getDateFromUser}>
                <Text>
                {
                    this.state.dateSelected.getDate() + "/"
                    + (this.state.dateSelected.getMonth() + 1) + "/"
                    + this.state.dateSelected.getFullYear()
                }
                </Text>
            </TouchableHighlight>
        );
    }
}

class DatePicker extends Component {
    constructor(props) {
        super(props);

        this.datePicker = Platform.select({
            ios: { Component: DatePickerIOS, Props: {} },
            android: { Component: DatePickerAndroidComponent, Props: { onDateSelected: (date) => console.log("Usuario selecionou a data", date.toISOString()) } },
        });
    }

    render() {
        const DatePicker = this.datePicker;
        return (
            <View>
                <Text>Selecione uma data:</Text>
                <DatePicker.Component {...DatePicker.Props} />
            </View>
        );
    }
}

export default DatePicker;
```

Repare no construtor do componente `DatePicker`. Lá, eu simplesmente chamo o método estático `Platform.select()` e dou as instruções sobre que componente utilizar para iOS e Android. Esse método também aceita MacOS, Web e outros tipos de plataforma. [Veja a documentação oficial para mais detalhes](https://facebook.github.io/react-native/docs/platform-specific-code).

O que usei no componente para o `DatePickerAndroid` é uma `View` que suporta detectar toques do usuário (tanto que o nome é `TouchableHighlight`). Ao tocar nesse componente, ele executa a função passada pela *prop* `onPress`, além de brilhar para reagir à ação do usuário.

Se sua aplicação é nova, você pode salvar este componente num arquivo separado e importá-lo no `App.js`:

```jsx
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import DatePicker from "./caminho/para/DatePicker";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <DatePicker />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
```

O resultado será algo assim:

<img src="img/react-native-datepicker-example.jpg" width="480" height="720">

Com isso, você aprendeu o suficiente para fazer uma aplicação de uma única página em React Native. :)

Para fazer mais páginas, leia o arquivo que fala sobre roteamento.

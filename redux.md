Integrando a aplicação com o Redux
==================================

*Redux* é uma *framework* javascript que serve para formalizar o fluxo de uma aplicação. Em outras palavras, ele serve para que os desenvolvedores sejam "forçados" a seguir o protocolo exigido, deixando sua aplicação mais organizada e escalável.

Existem diversas *frameworks* que dão suporte à integração com *Redux*, dado todo seu potencial. *React*, e consequentemente *React Native*, são exemplos de *frameworks* que suportam essa integração.

Para instalar, precisamos adicionar alguns pacotes extras à aplicação:

```bash
npm install --save redux react-redux
```

O pacote `redux` contém a *framework* do *Redux*, enquanto o pacote `react-redux` é responsável pela integração entre os dois.

Você pode seguir [este guia](https://react-redux.js.org/introduction/quick-start) para instalá-lo em sua aplicação, ou continuar lendo este documento.

O que faremos aqui é:

0. [Entender o que é *Redux*](#resumo-sobre-redux);

1. [Instalar e integrar o *Redux* com o *React*](#integrando-redux-e-react);

2. [Integrar o *Redux* com o *Reactotron*](#integrando-o-redux-com-o-reactotron);

3. [Integrar o *Redux* com o *React Router Native*](#integrando-o-redux-com-o-react-router-native).

Resumo sobre *Redux*
====================

A *framework Redux* formaliza um protocolo que você deve seguir para montar o fluxo de dados na sua aplicação. Esse "protocolo" utiliza três entidades principais: a ***store***, as **ações** e os ***reducers***.

Se quiser entender o que são essas entidades, leia os próximos subtópicos. Se não quer, pule para a [seção seguinte](#integrando-redux-e-react)

*Store*
-------

A ***store*** é uma entidade que atua como "o mestre dos dados". É nela que (em teoria) todos os dados devem ficar. Ou seja, é só uma variável (um objeto, especificamente) que guarda tudo.

Mas claro que esse não é o único objetivo da ***store***. O objetivo principal de globalizar os dados vem do objetivo de forçar você a seguir o "protocolo". Ou seja, todos os dados estão lá, globais e facilmente acessíveis, mas para acessá-los deve-se seguir as regras do jogo.

As regras de alteração são dadas pelas **ações** e ***reducers*** (um trabalho conjunto), enquanto as regras de controle (*i.e.* o que seu componente pode alterar) são dadas pelos ***reducers***.


*Ações*
-------

As ações são entidades que dizem **como** você altera o dado da *store*. Quem recebe essa ação e realiza a alteração é o ***reducer***.

Quem chama uma ação é um componente. Quem executa as alterações pedidas pela ação é o ***reducer***.

Por exemplo, se na *store* você tem um contador específico de um componente, para alterá-lo você cria uma ação com um nome decente, *e.g.* **incrementar**, e programa seu ***reducer*** para incrementar o contador toda vez que essa ação for chamada pelo seu componente.

*Reducer*
---------

Um *reducer* é uma função. Os parâmetros são (estado_atual, ação_executada), e o resultado é (próximo_estado). O estado atual de um *reducer* quer dizer o estado atual da *store* (geralmente parte dele).

O *reducer* vê qual é o estado atual, qual ação foi executada, e retorna o próximo estado, que será guardado de volta na *store*.

Quantos *reducers* sua aplicação terá depende de quem estiver programando. Geralmente é feito um *reducer* para cada componente, para manter as coisas pequenas e bem organizadas, mas o número de arquivos aumenta bastante em contrapartida.

Integrando *Redux* e *React*
============================

Depois de instalar os pacotes `redux` e `react-redux`, precisamos aprender a usá-los. Nossos objetivos são:

1. Criar a *store*;

2. Ligar a *store* com a aplicação;

3. Aprender a conectar nossos componentes com a *store*;

4. Aprender a criar ações e *reducers*.

Criando a store
---------------

Para criar a *store*, precisamos ter ao menos um *reducer* para repassar estados para ela, porque não faz sentido ter uma *store* vazia, sem meios de alterá-la.

Então, vamos criar um *reducer* "vazio" para repassar para nossa *store*, por enquanto.

O que vou fazer para os próximos exemplos é criar uma pasta chamada `store/` dentro da pasta `ui/`, e colocar lá dentro os arquivos `reducers.js` e `store.js`, com esse conteúdo:

```js
/* ui/store/reducers.js */
import { combineReducers } from "redux";

const initialState = {};

function globalReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
    }
}

export default combineReducers({
    dummy: globalReducer,
});
```

```js
/* ui/store/store.js */
import { createStore } from "redux";
import reducers from "./reducers";

export const store = createStore(reducers);
```

Veja a função `globalReducer()` em `reducers.js`. Ele é seu primeiro e mais simples *reducer* que você verá. Todos eles podem ser programados desta forma: o primeiro argumento é o estado atual (que inicia com o estado inicial contido na variável `initialState`), e o segundo é a ação disparada. Essa ação pode ser o que você quiser, mas vou pedir que siga um padrão mais tarde. :)

Ainda em `reducers.js`, nós exportamos o resultado da função `combineReducers()`, que você provavelmente sabe o que é (dica: tem a ver com todos os reducers combinados em um só). Como só temos um unico *reducer*, essa chamada acaba sendo redundante, mas vamos deixá-la ali assim mesmo.

O nome do nosso *reducer* global será *dummy* por enquanto, porque não temos nenhum objetivo para ele.

A função `createStore()` em `store.js` recebe até três argumentos. O primeiro é uma composição de *reducers*, que será o que exportamos em `reducers.js`. O segundo é uma composição de *middlewares* e *enhancers*, que servem para adicionar funcionalidades à *store*. O terceiro é o estado inicial da *store* em si.

Você verá em muitos projetos a presença de um *middleware* chamado [*Redux Thunk*](https://github.com/reduxjs/redux-thunk). É um conceito bem interessante, mas que não veremos aqui. Dê uma olhada na [documentação oficial](https://github.com/reduxjs/redux-thunk) para mais informações.

Ligando a *store* com a aplicação
---------------------------------

Agora que temos nossa *store* pronta, podemos ligá-la à aplicação pelo `App.js`:

```js
/* App.js */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import { NativeRouter, Route, Switch } from "react-router-native";

import { Provider } from "react-redux";
import { store } from "./ui/store/store";

import Layout from "./ui/layout/Layout";
import Home from "./ui/screens/home/Home";
import About from './ui/screens/about/About';
import PageNotFound from "./ui/screens/errors/PageNotFound";

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <NativeRouter>
                    <Layout>
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route exact path="/about" component={About} />
                            <Route render={() => <PageNotFound />} />
                        </Switch>
                    </Layout>
                </NativeRouter>
            </Provider>
        );
    }
}
```

O componente que conecta a *store* é o `<Provider>` do pacote `react-redux`. Ele aceita uma *prop* chamada *store*, que é por onde passamos a *store* que acabamos de criar.

Note que o `<Provider>` deve ser o componente raíz, para que ele possa "repassar" a *store* por todos os componentes em sua hierarquia.

Conectando a *store* com componentes
------------------------------------

O conceito de "conectar" a *store* à um componente é o ato de dar acesso a um pedaço da *store* ao componente, e permitir ou não que ele altere esse pedaço através de ações.

Para entender melhor, vamos trabalhar num exemplo. Vou criar um novo *reducer* para nossa página *Home*, que será um contador simples.

Entretanto, porém, antes disso, precisamos definir que ações nosso contador vai ter. Por isso, vou criar um arquivo chamado `actions.js` dentro de `ui/screens/home`:

```js
/* ui/screens/home/actions.js */
export const HomeActionTypes = {
    INCREMENT: "HomeActionTypes.INCREMENT",
    DECREMENT: "HomeActionTypes.DECREMENT",
    RESET: "HomeActionTypes.RESET",
};
```

Por enquanto, esse arquivo só conterá as definições das ações do nosso `reducer()`. O nome da constante é ARBITRÁRIO: coloquei o meu como `HomeActionTypes` porque ele está relacionado ao nosso `reducer()`, **cujo nome também é arbitrário**.

Claro que existem algumas convenções: Para o nome da constante que define as ações, geralmente usam o sufixo `ActionTypes` para explicitar o que é essa constante. Agora repare nas *strings* que coloquei, usando o formato `"HomeActionTypes.<nome_da_ação>"`.

Essas *strings* devem ser **únicas**. Os *reducers* usam essas *strings* para diferenciar que ação foi disparada. Se houver duas constantes que utilizam uma mesma *string*, os *reducers* que utilizam essas *strings* não vão saber diferenciar que ação foi disparada, e todos vão reagir.

Bom, agora que temos nossos tipos de ações, podemos criar nosso *reducer* (que vou criar em `ui/screens/home/` e chamarei de `reducer.js`):

```js
/* ui/screens/home/reducer.js */
import { HomeActionTypes } from "./actions";

const initialState = {
    count: 0,
};

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case HomeActionTypes.DECREMENT:
            return { ...state, count: state.count - 1 };
        case HomeActionTypes.INCREMENT:
            return { ...state, count: state.count + 1 };
        case HomeActionTypes.RESET:
            return initialState;
        default:
            return state;
    }
}
```

Dê uma analisada no *switch* do nosso *reducer*. O que ele faz é super simples: Se a ação `INCREMENT` for disparada, ele aumenta a contagem em 1. Se a ação `DECREMENT` for disparada, ele decrementa essa contagem. Se a ação `RESET` for disparada, ele retorna ao estado inicial. Se uma ação **desconhecida** for disparada, ele mantém o estado atual.

**Ações desconhecidas** são ações de outros *reducers*. Lembra que uma ação disparada passa por todos os *reducers*? Bom, se isso acontece, então certamente haverá muitos casos em que seu *reducer* não precisará fazer nada.

O último passo para criar o *reducer* é adicioná-lo na lista de *reducers* da *store*, em `ui/store/reducers.js`:

```js
/* ui/store/reducers.js */
import { combineReducers } from "redux";

import homeReducer from "../screens/home/reducer";

const initialState = {};

function globalReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
    }
}

export default combineReducers({
    dummy: globalReducer,
    home: homeReducer,
});

```

Note que importei nosso *reducer* com o nome `homeReducer`, só para evitar problemas de nomenclatura no futuro. Esse nome é **arbitrário**. Para a *store*, o `homeReducer` será nomeado como `home`, que também foi escolhido **arbitrariamente**.

Finalmente, podemos conectar nossa *store* com mpsso componente `Home`:

```js
/* ui/screens/home/Home.js */
import React, { Component } from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";

class Home extends Component {
    render() {
        return (
            <View>
                <Text>Home</Text>
                <Text>Count: {this.props.count}</Text>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    count: state.home.count,
});

export default connect(mapStateToProps)(Home);
```

Aqui, precisamos entender duas coisas: **1)** a função `mapStateToProps()` e **2)** a função `connect()` do pacote `react-redux`.

Vou começar pela função `mapStateToProps()`. Essa função deve ser passada como primeiro argumento da função `connect()`.

O que ela faz é permitir o mapeamento da *store* para as *props* do componente.

O que ela retorna, no meu caso, é um mapeamento do atributo `count` do estado do nosso `homeReducer()` para a *prop* que chamei de `count`, resultanto em `count: state.home.count`.

Para saber o que tem dentro do parâmetro *state* dessa função, basta ver o que você colocou na lista de *reducers* da *store*, que no meu caso é:

```js
export default combineReducers({
    dummy: globalReducer,
    home: homeReducer,
});
/*
    reducers: {
        dummy: globalReducer,
        home: homeReducer,
    }
 */
```

A *store* usa esses nomes que damos aos *reducers* para guardar o estado atual da aplicação, que é passada como o parâmetro *state* da função `mapStateToProps()`.

O mapeamento é simples: Lembra que o `initialState` do `globalReducer()` era um objeto vazio `{}`? E que o `initialState` do `homeReducer()` era o objeto `{ count: 0 }`?

Então, o mapeamento que a *store* vai fazer é esse:

```js
state = {
    dummy: {}, /* vem do globalReducer() */
    home: { count: 0 }, /* vem do homeReducer() */
};
```

E isso explica porque eu utilizei `state.home.count` dentro da função `mapStateToProps()`.

E como mapeei esse atributo para a *prop* `count` do componente, eu posso acessá-lo via `this.props.count`.

Apesar de ser um mapeamento simples, você deve estar achando que foi um baita trabalho só para conseguir acesso à esse atributo, certo? Esse é o protocolo de acesso do *Redux*, que literalmente lhe força a seguí-lo.

Se todos seguirem o protocolo, todos conseguirão ler qualquer código da aplicação. Isso facilita a manutenção depois.

Criando ações
-------------

Esta última parte deveria ser sobre ações e *reducers*, mas como fomos obrigados a criar tipos de ações e um *reducer* na última seção, só nos falta falar das ações propriamente ditas.

Uma ação é um objeto que você "despacha" (ou "dispara") usando uma função especial do *Redux* chamada `dispatch()`. O formato desse objeto (que atributos vai ter) é **arbitrário**, mas vou pedir que siga um padrão para que ninguém fique perdido desenvolvendo.

O padrão vai ser simples: toda ação será um objeto que tem dois atributos: um `type`, que vai ser uma string com o tipo da ação, e um `payload`, que vai conter os dados dessa ação (se existirem!).

Então, vamos criar nossas primeiras ações, que vai disparar os tipos que criamos em `HomeActionTypes`. Abra seu arquivo `ui/screens/home/actions.js` se você estiver seguindo este tutorial e adicione o seguinte:

```js
/* ui/screens/home/actions.js */
export const HomeActionTypes = {
    INCREMENT: "HomeActionTypes.INCREMENT",
    DECREMENT: "HomeActionTypes.DECREMENT",
    RESET: "HomeActionTypes.RESET",
};

export function increment() {
    return { type: HomeActionTypes.INCREMENT };
}

export function decrement() {
    return { type: HomeActionTypes.DECREMENT };
}

export function reset() {
    return { type: HomeActionTypes.RESET };
}
```

O que fiz foi criar três funções que retornam cada tipo de ação. Note que nenhuma delas tem um *payload*, porque não temos nada de diferente para enviar para nosso *reducer*: você certamente sabe o que fazer se a ação for *incrementar*, sem precisar de um dado extra. :)

Agora, adicionamos essas ações para a página `Home`:

```js
/* ui/screens/home/Home.js */
import React, { Component } from "react";
import { View, Text, Button } from "react-native";
import { connect } from "react-redux";

import { increment, decrement, reset } from "./actions";

class Home extends Component {
    render() {
        return (
            <View>
                <Text>Home</Text>
                <Text>Count: {this.props.count}</Text>
                <Button onPress={this.props.increment} title="Increment" />
                <Button onPress={this.props.decrement} title="Decrement" />
                <Button onPress={this.props.reset} title="Reset" />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    count: state.home.count,
});

const mapDispatchToProps = {
    increment, decrement, reset
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
```

Agora, vamos utilizar o segundo argumento da função `connect()`, que é o `mapDispatchToProps()`.

Esse argumento pode ser um **objeto** ou uma **função**.

* Se for um **objeto**, cada atributo deve ser **uma ação**, ou seja, **uma função que retorna um objeto de uma ação**. O que a *store* faz nesse caso é repassar uma *prop* com o mesmo nome que sua ação (*e.g.* `this.props.increment`), que realiza o `dispatch()` do retorno dessa ação (já explico melhor);

* Se for uma **função**, a *store* passará como parâmetro a função `dispatch()`, que pode ser repassada para suas ações.

Eis um exemplo que mostra os dois tipos de `mapDispatchToProps()` que você pode fazer (ambos são equivalentes):

```js
/*
    como o nome dos atributos é igual ao valor, não precisa fazer "increment: increment".
    você pode simplificar para "increment".
 */
const mapDispatchToProps = {
    increment: increment,
    decrement: decrement,
    reset: reset, 
}

/*
    o (...args) é desnecessário nesse caso, porque nenhuma dessas ações precisa de argumentos.
    mas é (mais ou menos) isso que a store faz se você retorna o objeto acima para ela com a função connect().
 */
const mapDispatchToProps = (dispatch) => ({
    increment: (...args) => dispatch(increment(...args)),
    decrement: (...args) => dispatch(decrement(...args)),
    reset: (...args) => dispatch(reset(...args)),
});

/* esse é IGUAL ao de cima, mas usando uma function ao invés de uma arrow function */
const mapDispatchToProps = function (dispatch) {
    return {
        increment: (...args) => dispatch(increment(...args)),
        decrement: (...args) => dispatch(decrement(...args)),
        reset: (...args) => dispatch(reset(...args)),
    };
};

/*
    ambos mapeamentos vão chamar a função dispatch desse jeito:
    dispatch({ type: HomeActionTypes.INCREMENT });
    dispatch({ type: HomeActionTypes.DECREMENT });
    dispatch({ type: HomeActionTypes.RESET });
 */
```

Se hoje você não entender, vá desenvolvendo aplicações com *Redux* e volte outra vez para ver o quanto você amadureceu. :)

Só para dar um exemplo de uso do `payload`, aqui estão alguns pedaços de como utilizá-lo:

```js
/* ui/screens/home/actions.js */
export const HomeActionTypes = {
    INCREMENT: "HomeActionTypes.INCREMENT",
    DECREMENT: "HomeActionTypes.DECREMENT",
    RESET: "HomeActionTypes.RESET",
    ADD: "HomeActionTypes.ADD",
    UPDATE_ADD_FIELD: "HomeActionTypes.UPDATE_ADD_FIELD",
};

/* ... */
export function add(amount) {
    return { type: HomeActionTypes.ADD, payload: amount };
}

export function updateAddField(fieldValue) {
    return { type: HomeActionTypes.UPDATE_ADD_FIELD, payload: fieldValue };
}
```

```js
/* ui/screens/home/reducer.js */
export default function reducer(state = initialState, action) {
    switch(action.type) {
        /* ... */
        case HomeActionTypes.UPDATE_ADD_FIELD:
            return { ...state, addField: action.payload };
        case HomeActionTypes.ADD:
            return { ...state, count: state.count + action.payload };
        default:
            return state;
    }
}
```

```js
/* ui/screens/home/Home.js */
import React, { Component } from "react";
import { View, Text, Button, TextInput } from "react-native";
import { connect } from "react-redux";

import { increment, decrement, reset, add, updateAddField } from "./actions";

class Home extends Component {
    add = () => this.props.add(parseInt(this.props.addField));
    render() {
        return (
            <View>
                <Text>Home</Text>
                <Text>Count: {this.props.count}</Text>
                <Button onPress={this.props.increment} title="Increment" />
                <Button onPress={this.props.decrement} title="Decrement" />
                <Button onPress={this.props.reset} title="Reset" />
                <TextInput keyboardType="numeric" onChangeText={this.props.updateAddField} />
                <Button onPress={this.add} title="ADD" />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    count: state.home.count,
    addField: state.home.addField,
});

const mapDispatchToProps = {
    increment, decrement, reset, add, updateAddField
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
```

Com isso, finalmente terminamos a item 1 da nossa lista, que é "Instalar e integrar o Redux com o React".

Integrando o *Redux* com o *Reactotron*
=======================================

O *Reactotron* possui um pacote pronto para facilitar nossa vida:

```bash
npm install --save reactotron-redux
```

Depois de baixar o pacote, altere seu arquivo `ReactotronConfig.js`:

```js
/* ReactotronConfig.js */
import Reactotron from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";
import env from "./env";

const reactotron = Reactotron
  .configure({
    host: env.ip
  }) // controls connection & communication settings
  .use(reactotronRedux())
  .useReactNative() // add all built-in react native plugins
  .connect() // let's connect!

console.tron = __DEV__ ? reactotron : () => {};

export default reactotron;
```

A parte `.use(reactotronRedux())` vai aplicar o *plugin* à instância do Reactotron que será guardada na constante `reactotron`. Nós damos um *export* nessa variável para podermos utilizá-la no arquivo `store.js`:

```js
/* ui/store/store.js */
import { createStore, compose } from "redux";

import reducers from "./reducers";

const middlewares = [];

if (__DEV__) {
    const Reactotron = require("../../ReactotronConfig.js").default;
    middlewares.push(Reactotron.createEnhancer());
}

export const store = createStore(reducers, compose(...middlewares));
```

Note que adicionamos um *middleware* do Reactotron na criação da *store*. Isso vai fazer com que todas as ações despachadas sejam logadas no Reactotron:

![Janela do Reactotron depois de integrar com o Redux](img/redux-integrated-with-reactotron.png "Janela do Reactotron depois de integrar com o Redux")

Ter essa *feature* é extremamente útil para desenvolvedores, porque se você encontrar um *bug* na tarefa de seu colega, você pode simplesmente enviar para ele a sua *timeline*, para ele poder repetir seus passos e recriar o *bug*.

Você também não precisa estar prestando atenção em todos os passos que você faz, porque tudo estará sendo logado na *timeline*. :)

Integrando o *Redux* com o *React Router Native*
================================================

Essa seção será mais sobre explicações.

Se você pesquisar por meios de integrar o *Redux* com o *React Router*, você provavelmente irá cair:

* [No repositório do pacote `react-router-redux`](https://github.com/reactjs/react-router-redux);

* Na [documentação oficial do React Router](https://reacttraining.com/react-router/web/guides/redux-integration).

Ambos são contraditórios. A documentação oficial do *React Router* **não recomenda** que você use pacotes como o `react-router-redux` para criar uma "integração profunda".

Por isso, não vou ensinar como utilizá-lo, mas o único efeito que o pacote `react-router-redux` gera é mostrar no *log* (ou Reactotron, no nosso caso) todas as mudanças de página que ocorrerem na aplicação.

Isso não chega a ser necessário, e as vezes torna a *timeline* desnecessariamente suja.

A documentação oficial diz para você simplesmente utilizar o conector do *React Router* em seus componentes conectados com o *Redux*:

```js
// antes
export default connect(mapStateToProps)(MeuComponente)

// depois
import { withRouter } from 'react-router'
export default withRouter(connect(mapStateToProps)(MeuComponente))
```

Você só precisa do pacote `react-router` para conectar seu componente através da função `withRouter()`. Ela vai repassar as *props* `location`, `history` e `match`, conforme descrito no guia sobre o *React Router*.

Em resumo, minha recomendação é que você evite utilizar pacotes que tentam realizar uma "integração profunda" entre o *Redux* e o *React Router*, porque isso traz poucos benefícios.
